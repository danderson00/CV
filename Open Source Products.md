# Open Source Products - Dale Anderson

## tribe.js

Tribe is an application development platform that changes the way we build distributed applications. It allows developers to express application logic purely in terms of expressions over streams of events. 

For example, in a game of table tennis, a player is deemed the winner when they have scored 11 or more points and have a lead of 2 or more points. Tribe allows this rule to be expressed in a more natural way, bringing us closer to executable specifications and user interfaces driven by visual state machines. Offline applications are inherently simple to build due to event buses residing both on the device and on the server.

The current version of the platform has not been updated in some time and is using older UI technology (knockout.js) and currently only runs on an older version of Node.js. I will be reimplementing the framework using react in the near future.

- github: https://github.com/danderson00/tribe
- npm: https://www.npmjs.com/package/tribe
- Website: http://tribejs.com/ (Note that this website targets an earlier version that used ASP.NET server-side instead of Node.js)
- Live sample: http://score-dev.azurewebsites.net/


## xstor

xstor is a cross platform, indexable object storage library that provides a simple interface for storing and querying objects across any Javascript platform, including browser, Node.js, react native and cordova.

- github: https://github.com/danderson00/xstor
- npm: https://www.npmjs.com/package/xstor


## xtagcloud

xtagcloud makes generating beautiful, animated word clouds simple across any Javascript platform. Documentation will be added in the near future.

- github: https://github.com/danderson00/xtagcloud
- npm: https://www.npmjs.com/package/xtagcloud


## webpack-worker

The webpack-worker product makes building CPU intensive Javascript applications considerably simpler by placing a simple, promise based API around WebWorkers.

- github: https://github.com/danderson00/webpack-worker
- npm: https://www.npmjs.com/package/webpack-worker
- Blog post: https://github.com/danderson00/webpack-worker/blob/master/samples/plotly/blog.md
- Live sample: https://danderson00.github.io/webpack-worker/


## DynamicOData

The DynamicOData library converts OData filter queries into IQueryable objects that encapsulate a query against a remote database, without requiring compiled C# classes.

- github: https://github.com/danderson00/DynamicOData
- nuget: https://www.nuget.org/packages/DynamicOData/


## isstopword

A simple, efficient function for determining if a word is an English [stop word](https://en.wikipedia.org/wiki/Stop_words).

- github: https://github.com/danderson00/isstopword
- npm: https://www.npmjs.com/package/isstopword


## test-studio

Test Studio is a browser based unit testing UI for browser and Node.js applications, featuring individual test runs, simple step-into functionality and hot reloading. It is built on tribe.js technology.

The product is no longer being maintained due to significant advances in products such as Visual Studio Code.

- github: https://github.com/danderson00/tribe/tree/master/node_modules/tribe/test-studio
- npm: https://www.npmjs.com/package/test-studio


## PackScript

Powerful resource build system that combines and minifies your Javascript, HTML and CSS files based on Javascript configuration files.

The project is no longer being maintained due to the availability of other excellent, well supported products such as webpack and rollup.

- github: https://github.com/danderson00/packscript
- npm: https://www.npmjs.com/package/packscript
- Website: http://packscript.com/ 

