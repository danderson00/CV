There are three curriculum vitae formats:

- [full](https://github.com/danderson00/misc/blob/master/CV%20-%20Dale%20Anderson%20-%20full.docx?raw=true) - The full CV in Microsoft Word (.docx) format
- [online](https://github.com/danderson00/misc/blob/master/CV%20-%20Dale%20Anderson%20-%20online.md) - The full CV in markdown (.md) format
- [summary](https://github.com/danderson00/misc/blob/master/CV%20-%20Dale%20Anderson%20-%20summary.docx?raw=true) - A 2 page CV summary in Microsoft Word (.docx) format

Click [here](https://github.com/danderson00/misc/blob/master/CV%20-%20Dale%20Anderson%20-%20online.md) to view the online version.

Click [here](https://github.com/danderson00/misc/blob/master/Open%20Source%20Products.md) to see details on the various open source products built by Dale.