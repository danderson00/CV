# Curriculum Vitae for Dale Anderson

Full name: Dale Alyn Anderson

Nationality: Australian

Phone: +61 410 430 108

Email: [danderson00@gmail.com](mailto:danderson00@gmail.com)

Blog: [http://www.danderson00.com/](http://www.danderson00.com/)

LinkedIn: [http://linkedin.com/in/danderson00/](http://linkedin.com/in/danderson00/)

Twitter: [http://twitter.com/danderson00/](http://twitter.com/danderson00/)

Github: [http://github.com/danderson00/](http://github.com/danderson00/)

## Summary

Dale is an exceptionally passionate software engineer with a career spanning over 16 years, primarily focused around the Microsoft technology stack, node.js and open source web technologies. He has a track record of delivering successful outcomes in software engineering, solution architecture and team leadership for a large variety of corporate and government clients.

Dale is a thought leader in the industry and has produced a number of cutting edge open source products for building high performance distributed web and mobile apps based on highly scalable message based architectures. More details can be found at [http://www.tribejs.com/](http://www.tribejs.com/), [https://www.npmjs.org/package/test-studio](https://www.npmjs.org/package/test-studio), [http://www.packscript.com/](http://www.packscript.com/) and [http://www.danderson00.com/](http://www.danderson00.com/).

Dale also has sound experience in the execution of agile methodologies. He has the ability to lead teams from project conception through to success using proven agile processes, leading to higher product quality and customer satisfaction.

## Technical Skills

### Programming Technologies

| | |
|---|---|
|Microsoft .NET (1.0-4.5) | 14 years |
|HTML / CSS / JavaScript | 16 years |
|Node.js | 5 years |
|JavaScript frameworks (react, knockout, jQuery, backbone, etc.) | 8 years |
|Hybrid mobile apps, Apache Cordova | 3 years |
|Cloud technology (Azure, Amazon, Google) | 7 years |
|Unit, integration and functional testing tools | 10 years |
|Source control / ALM (TFS, git, SVN, VSS) | 15 years |
|Microsoft SQL Server 2000/5/8/12 | 16 years |
|Non-relational (No-SQL) databases | 6 years |
|Traditional web and desktop presentation technologies (ASP.NET MVC, WPF / Silverlight) | 14 years |
|Service layer technologies (WCF, ASP.NET WebApi) | 9 years |
|Messaging patterns and technologies (MSMQ, NServiceBus) | 7 years |
|Continuous integration tools | 10 years |
|Unit, integration and functional testing tools | 10 years |

### Architectural Practices, Patterns and Tools

Extensive experience with and expert knowledge of:

- applying functional programming techniques in Javascript and C#, particularly using the Rx framework
- highly scalable message driven architectures such as CQRS,
- service oriented architectures (SOA),
- n-tier architectural models,
- relational and non-relational data storage models,
- presentation patterns such as MVC, MVVM and MVP,
- unit, integration and functional testing,
- source control and Application Lifecycle Management,
- build automation and continuous integration,
- performance, optimisation and code quality tools.

### Development Methodologies

Extensive experience with end to end project life cycles using both agile (lean, scrum, XP) and traditional waterfall methodologies, including:

- scoping and project definition,
- requirements gathering,
- value driven task prioritisation,
- agile estimation and planning.

### Leadership / Management

Extensive experience with:

- client collaboration,
- talent acquisition for technical and analyst roles,
- mentoring and training of team members,
- planning and estimation using both agile and traditional methodologies,
- preparing and delivering technical and customer presentations,
- chairing team and client facing meetings,
- project monitoring and reporting.

### Analysis / Design

Extensive experience with:

- domain driven, activity based, data and process driven design,
- requirements gathering,
- rapid prototyping and wireframing,
- process reengineering,
- production of specifications, technical and user documentation.

## Employment History

**Mar 2015 to Apr 2017 - Microsoft - Senior Software Engineer**

Hired for my Node.js and Javascript expertise, my initial responsibility was primarily to rebuild the existing Azure Mobile Services server product, leading a small team, into an SDK that is now known as Azure Mobile Apps.

Azure Mobile Apps is an SDK designed to build distributed mobile applications with offline data sync, push notifications and authentication. The SDK was released as a preview in September 2015 and became generally available to the public in February 2016. Since the initial release, the SDK has undergone 32 releases and had significant capabilities added, such as integration with Azure Blob Storage.

More recently, the team was split and I became the technical lead for the entire product. I integrated the product into Microsoft&#39;s new mobile developer portal, Visual Studio Mobile Center. This involved building the front end using react and the back end that communicates with Azure Resource Management. The integration was successfully produced with an extremely tight deadline.

For more information on the products, please see [https://github.com/Azure/azure-mobile-apps-node](https://github.com/Azure/azure-mobile-apps-node) and [https://mobile.azure.com/](https://mobile.azure.com/).

Technologies used: Node.js, Javascript, C#, react, mobx, SQL Server, SQLite, DocumentDB

**Oct 2011 to Dec 2014 - Sporting Tribe Pty Ltd - Founder and Managing Director**

Sporting Tribe is a product that provides broadcast quality scoring and statistical services to sports players. Significant R&amp;D has been invested into producing a development platform that facilitates building distributed, truly cross-platform applications.

The platform, currently known as tribe, is built entirely on Node.js and allows developers to express domain objects in terms of events that occur within the application. The platform allows applications to be built entirely on expressions over streams of messages – this eliminates persistence concerns and enables seamless, real time distribution of data to any device with a web browser. It also provides a comprehensive composite UI framework built on knockout.js.

For more information on the platform, see [https://github.com/danderson00/tribe](https://github.com/danderson00/tribe).

The Sporting Tribe business is currently commercially inactive due to operating constraints, but development of the platform and product is ongoing.

Technologies used: Node.js, HTML5, CSS3, JavaScript, knockout.js, git
(initially C#, SignalR, NServiceBus)

**Sep 2013 to Oct 2014 - ActewAGL - Software Developer (Contract, 2 renewals)**

- Wrote highly complex T-SQL queries to extract and transform data,
- Performed SQL Server (2008, 2012) optimisation, including on tables with ~750 million rows,
- Designed and developed third party product integration layer using WCF,
- Designed and developed meter reading migration tool and complaints resolution tool as single page web applications built using SQL Server, WebAPI and knockoutjs,
- Provided advice on SDLC implementation.

Technologies used: C#, HTML, CSS, JavaScript, SQL Server, WCF, WebAPI, knockoutjs, TFS

**Jul 2010 to Oct 2011 - Fujitsu Australia - Solution Architect (Permanent)**

- Defined system architecture for second and subsequent releases of the BICON enterprise application for DAFF,
- Provided leadership, mentoring and technical guidance to a team of 14 developers, 5 business analysts and 5 testers throughout several releases of the project,
- Prepared and presented training sessions to a wider Fujitsu audience on coding practice, unit testing techniques and tools, language features and performance issues,
- Designed and implemented architectural components and complex features of the solution,
- Performed performance testing and optimisation of all layers of the system using performance and memory profiling tools.

Technologies used: C#, WCF, Entity Framework, ASP.NET MVC, HTML/CSS/JS, NServiceBus, Unity, AutoMapper, TFS

**Oct 2009 to Jul 2010 - Fujitsu Australia - Solution Architect / Technical Team Lead (Contract, 2 renewals)**

- Defined system architecture for second release of the BICON enterprise application for DAFF, consisting of a web service, desktop client, internal and external websites and externally visible web services. 
- Integration and messaging between components of the system is facilitated by an enterprise message bus.
- Provided technical guidance to a team of 14 developers, 5 business analysts and 5 testers,
- Designed and implemented architectural components and complex features of the solution.

Technologies used: C#, WCF, Entity Framework, ASP.NET MVC, HTML/CSS/JS, NServiceBus, Unity, AutoMapper, TFS

**Feb 2009 to Jun 2009 - Department of Education, Employment and Workplace Relations - Senior Developer / Team Leader (Contract, 1 renewal)**

- Developed complex functionality on enterprise web application and framework. The application used cutting edge AJAX techniques to create rich custom UI controls.
- Mentored team members on development principles and techniques,
- Developed unit, integration and functional tests,
- Allocated tasks and monitored progress,
- Identified code quality issues and process deficiencies, implemented solutions such as architecture improvements and developer education,
- Produced detailed design documentation.

Technologies used: C#, ASP.NET WebForms, HTML/CSS/JS, SQL Server, Enterprise Library, AJAX Control Toolkit, NUnit, Selenium, TFS

**Apr 2007 to Sep 2008 - Essex County Council - Platform Developer / Development Team Leader (Contract, 2 renewals)**

- Development team leader of six developers implementing social care application,
- Developed enterprise architecture platform providing web services, ASP.NET web presentation tier, discrete domain layer and integration points for various purposes,
- Led requirements and estimating meetings, allocated work based on developer skill sets and mentored developers,
- Mentored junior developer for implementation of a custom reporting system for phone charges from multiple providers,
- Interviewed candidates for developer and business analyst roles,
- Solution designed using domain driven design and developed using an agile development methodology based on XP,
- Developed unit, integration and functional tests,
- Developed a custom reporting application for legacy HR data.

Technologies used: C#, ASP.NET MVC and WebForms, HTML/CSS/JS, SQL Server, NHibernate, Castle Windsor, K2 Blackpearl, NUnit, WatiN, TFS

**Aug 2006 to Apr 2007 - London Stock Exchange / Accenture - Build Team Leader (Contract, 1 renewal)**

- Managed build and deployment process for London Stock Exchange corporate website ( [www.londonstockexchange.com](http://www.londonstockexchange.com)), incorporating several bespoke products built by third party vendors,
- Implemented build process improvements and code quality initiatives,
- Provided mentoring and guidance to team members,
- Implemented risk management application,
- Evaluated potential new team members.

Technologies used: C#, CruiseControl.NET, NAnt, NUnit, FXCop, Clover.NET, Selenium, ASP.NET WebForms, SQL Server, TFS

**Aug 2004 to May 2006 - Airservices Australia - Senior Analyst/Developer (Contract, 4 renewals)**

- Provided mentoring and guidance to colleagues on application development and design issues,
- Evaluated potential employees,
- Spent approximately three months in a formal business analyst role,
- Developed extensive application framework and trained colleagues on its use,
- Developed comprehensive set of development standards and guidelines,
- Lead developer and designer for radio frequency assignment software containing complex mathematical algorithms for geodetic calculations,
- Other projects included a workshop voting tool for PocketPCs, a custom workflow solution for document approval and an issues management system.

Technologies used: C#, ASP.NET WebForms, HTML/CSS/JS, .NET Compact Framework, Windows Forms, XML Web Services, SQL Server, Oracle, Enterprise Library, VSS

**May 2004 to Aug 2004 - One Planet Solutions / AusAID - Senior Software Developer (Contract, complete)**

- Senior developer on development team of about 20 for a large, web based student grants application system,
- Performed complex development tasks,
- Provided mentoring and advice to colleagues.

Technologies used: C#, ASP.NET WebForms, HTML/CSS/JS, SQL Server, VSS

**Nov 2001 to May 2004 - F1 Solutions - Software Developer / Architect (Permanent)**

- Provided mentoring to colleagues on analysis, design and development,
- Lead developer and designer on team of four for accreditation and finance management application,
- Developed security and performance enhancements to enterprise grant management system,
- Designed and developed scheduling and personnel management application,
- Enhanced web content management system,
- Enhanced radio dispatch and work management system.

Technologies used: C#, VB.Net, ASP.NET WebForms, HTML/CSS/JS, Windows Forms, XML Web Services, SQL Server, Enterprise Library, VB6, XSL-T, VSS

**Feb 2001 to Nov 2001 - WebOne Internet - Web Application Developer (Permanent)**

- Developed comprehensive website content management system ([www.paybysnap.com](http://www.paybysnap.com)),
- Developed several smaller web application projects including a survey system and searchable product database,
- Migrated customer accounts and services from ActWEB.NET,
- Provided technical and customer support.

Technologies used: HTML/CSS/JS, ASP, VB6, XSL-T, ActiveX

**Feb 2000 to Feb 2001 - ActWEB.NET - Technical Manager / Administrator (Permanent)**

- Managed infrastructure assets and services,
- Managed relationships with external service providers,
- Performed general network administration tasks,
- Maintained and enhanced web based customer account management system,
- Provided high level technical and customer support.

Technologies used: HTML/CSS/JS, OpenBSD, SQL Server, MySQL, PHP, PERL

## Education

**Advanced Distributed Systems Design**
Udi Dahan ([http://www.udidahan.com/training](http://www.udidahan.com/training))
(July 2011)

**Bachelor of Information Technology**
University of Canberra,
distinction average
(Graduated December 2003)

**Industrial Strength .NET**
Readify ([www.readify.net](http://www.readify.net))
(October 2005)

**Advanced .NET**
AppDev ([www.appdev.com](http://www.appdev.com))
(February 2003)


### References will be provided upon request
